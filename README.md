## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/mcaminero/python-be.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/mcaminero/python-be/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

---

# Python Back-End CRUD example

<details>
    <summary> Content </summary>
    <ul>
        <li><a href="#description"> Description</a></li>
        <li><a href="#methods"> Methods</a></li>
            <ul>
                <li><a href="#get-posts"> GET Posts</a></li>
                <li><a href="#post-posts"> POST Posts</a></li>
                <li><a href="#put-posts"> PUT Posts</a></li>
                <li><a href="#get-postspost_id"> GET Posts/post_id</a></li>
                <li><a href="#delete-postspost_id"> DELETE Posts/post_id</a></li>
            </ul>
        <li><a href="#installation"> Installation</a></li>
        <li><a href="#usage"> Usage</a></li>
        <li><a href="#support"> Support</a></li>
        <li><a href="#roadmap"> Roadmap</a></li>
        <li><a href="#authors-and-acknowledgment"> Authors and acknowledgment</a></li>
        <li><a href="#project-status"> Project Status</a></li>
    </ul>
</details>

## Description

This project functions as a CRUD example for any newcommer that wants to learn Back-End Python with Fastapi and Postgresql.

<p text-align="right">(<a href="#python-back-end-crud-example">back to top</a>)</p>

## Methods

### GET Posts

<img src = "image.png" alt ="Get Posts method" height = 400 >

This method doesn't need any parameters or body data to fuction.  
It retrieves all the posts from de Data Base and returns them as a List of Posts

<p text-align="right">(<a href="#python-back-end-crud-example">back to top</a>)</p>

### POST Posts

<img src="image-1.png" alt ="Post Posts method" height = 600>

This method requires a CreatePost object that contains a title and the content of the Post.  
It returns the created Post

<p text-align="right">(<a href="#python-back-end-crud-example">back to top</a>)</p>

### PUT Posts/{post_id}

<img src = "image-2.png" alt ="Put Posts/post_id method" height= 600>

This method requires the id of the Posts that the user wants to modify it, and the data that can be modified the title and the content.  
It returns the modified Post.

<p text-align="right">(<a href="#python-back-end-crud-example">back to top</a>)</p>

### GET Posts/{post_id}

<img src="image-3.png" alt ="Get Posts/post_id method" height = 500>

This method requires the id of the Post that the user wants to retrieve from the Data Base.  
It returns the Post with the same "post_id"

<p text-align="right">(<a href="#python-back-end-crud-example">back to top</a>)</p>

### DELETE Posts/{post_id}

<img src= "image-4.png" alt = "Delete Posts/post_id method" height = 500>

This method requires the id of the Post that the user wants to delete.  
This method doesn't reuturn anything.

<p text-align="right">(<a href="#python-back-end-crud-example">back to top</a>)</p>

## Installation

Every library that this project needs is listed inside the [requirements.txt](requirements.txt) file.  
Please follow the next steps:

1. If you don't have [Docker](https://www.docker.com/products/docker-desktop/) please install it.
2. Once Docker is installed, type the following command `docker-compose up -d --build`.
   For more info go to this [link](https://docs.docker.com/compose/compose-application-model/)
3. Once the web and the Data Base are running you are good to go!

<p text-align="right">(<a href="#python-back-end-crud-example">back to top</a>)</p>

## Usage

I recommend using [Postman](https://www.postman.com/) to work with this project. It doesn't have any browser interface yet.

<p text-align="right">(<a href="#python-back-end-crud-example">back to top</a>)</p>

## Support

If you want to add or modify this code please check it out [here](https://gitlab.com/mcaminero/python-be)!

<p text-align="right">(<a href="#python-back-end-crud-example">back to top</a>)</p>

## Roadmap

- [x] Create the venv (Virtual Environment) to use a python interpreter
- [x] Create the basic structure for the project.
- [x] Add the Data Base and the Models.
- [x] Separate the logic in different folders and files.
- [ ] Create an interface for a browser

<p text-align="right">(<a href="#python-back-end-crud-example">back to top</a>)</p>

## Authors and acknowledgment

### Author

#### Martin Caminero

### Contributors

- #### Patricio Prado

<p text-align="right">(<a href="#python-back-end-crud-example">back to top</a>)</p>

## Project status

Open

<p text-align="right">(<a href="#python-back-end-crud-example">back to top</a>)</p>
