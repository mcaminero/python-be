from fastapi import HTTPException, status, APIRouter, Depends
from src.users.crud import users_crud
from src.users.dto.users_dto import UserRequest, UserResponse
from src.utils import utils
from fastapi.security import OAuth2PasswordRequestForm
from src.deps import get_current_user, USER

route = APIRouter(prefix="/users", tags=["Users"])

unauthorized = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="invalid credentials",
)


def notfound(username: str):
    HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail=f"No user with username: {username}",
    )


@route.get("/")
async def get_users() -> list[UserResponse]:
    return [utils.from_user(user) for user in await users_crud.get_users()]


@route.post("/", status_code=status.HTTP_201_CREATED, response_model=UserResponse)
async def post_user(request: UserRequest) -> UserResponse:
    user = await users_crud.create_user(request)
    return utils.from_user(user)


@route.get("/{username}")
async def get_user_by_username(username: str) -> UserResponse:
    user = await users_crud.get_user_by_username(username)
    if user is not None:
        return utils.from_user(user)
    else:
        raise notfound(username)


@route.delete("/{username}")
async def delete_user(username: str):
    return await users_crud.delete_user(username)


@route.put("/{username}")
async def edit_user(username: str, request: UserRequest) -> UserResponse:
    user = await users_crud.edit_user(username, request)
    if user is not None:
        return utils.from_user(user)
    else:
        raise notfound(username)


# user get_current_user
@route.post("/token")
async def login(form_data: OAuth2PasswordRequestForm = Depends()) -> str:

    if (user := await users_crud.get_user_by_username(form_data.username)) is not None:
        return await users_crud.create_token(user)
    else:
        raise unauthorized


@route.get("/me/")
async def read_me(user: UserResponse = Depends(get_current_user)) -> UserResponse:
    return user
