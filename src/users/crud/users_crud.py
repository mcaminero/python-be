from src.users.db.mongo_db import engine
from src.users.model.user_model import User
from src.users.dto.users_dto import UserRequest
from src.core.security import create_access_token


async def get_users() -> list[User]:
    async with engine.session() as session:
        return await session.find(User)


async def create_user(request: UserRequest) -> User:
    async with engine.session() as session:
        return await session.save(User(**request.model_dump()))


async def get_user_by_username(username: str) -> "User|None":
    async with engine.session() as session:
        return await session.find_one(User, User.username == username)


async def delete_user(username: str):
    async with engine.session() as session:
        return await session.remove(User, User.username == username)


async def edit_user(username: str, request: UserRequest) -> "User|None":
    user = await get_user_by_username(username)
    if user is not None:
        async with engine.session() as session:
            user.model_update(request)
            return await session.save(user)
    else:
        return None


# utilizar security
async def create_token(user: User):
    return create_access_token(user)
