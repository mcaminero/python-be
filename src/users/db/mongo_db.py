from motor.motor_asyncio import AsyncIOMotorClient
from odmantic import AIOEngine
import os

client: AsyncIOMotorClient = AsyncIOMotorClient(host=os.getenv("MONGO_URL"))

engine = AIOEngine(client=client, database="user_db")
