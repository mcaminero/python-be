from pydantic import BaseModel


class UserRequest(BaseModel):
    name: str
    lastname: str
    username: str
    password: str


class UserResponse(BaseModel):
    name: str
    lastname: str
    username: str


class UserInDB(UserResponse):
    password: str


class UserTokenPayload(BaseModel):
    sub: str
    exp: int
    username: str
