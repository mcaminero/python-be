from odmantic import Model, Field


class User(Model):
    name: str
    lastname: str
    username: str = Field(unique=True)
    password: str
