import pytest
from unittest.mock import patch, MagicMock

# Assuming these are the imports in your module
from src.posts.crud.post_crud import delete_post, get_post_by_id


@patch("your_module.get_post_by_id")
@patch("your_module.session")
def test_delete_post(mock_session, mock_get_post_by_id):
    # Arrange
    post_id = 1
    mock_post = MagicMock()
    mock_get_post_by_id.return_value = mock_post

    mock_db_session = mock_session.return_value.__enter__.return_value
    mock_db_session.delete.return_value = None

    # Act
    delete_post(post_id)

    # Assert
    mock_get_post_by_id.assert_called_once_with(post_id)
    mock_db_session.delete.assert_called_once_with(mock_post)
    mock_db_session.commit.assert_called_once()
