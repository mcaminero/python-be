from jose import jwt
from src.users.model.user_model import User
from datetime import datetime, timedelta, timezone

ALGORITHM = "HS256"
SECRETKEY = "asdfagdfseh1234865456#$%$&/()"


def create_access_token(
    user: User,
    expires_minutes: int = 5,
) -> str:
    data = {
        "sub": str(user.name),
        "exp": datetime.now(tz=timezone.utc) + timedelta(minutes=expires_minutes),
        "username": str(user.username),
    }
    token = jwt.encode(data, SECRETKEY, algorithm=ALGORITHM)
    return token


def decode_token(token: str):
    return jwt.decode(token=token, key=SECRETKEY, algorithms=[ALGORITHM])
