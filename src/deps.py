from fastapi.security import OAuth2PasswordBearer
from src.users.model.user_model import User
from fastapi import Depends, HTTPException, status
from src.core.security import decode_token
from jose import JWTError
from pydantic import ValidationError
from src.users.dto.users_dto import UserTokenPayload
from src.users.crud.users_crud import get_user_by_username

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="")


# crear funcion de getuser usando el crud
# utilizo el security para decodear el token
async def get_current_user(token: str = Depends(oauth2_scheme)) -> User:

    unauthorized = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="invalid credentials",
    )

    try:
        payload = decode_token(token)
    except (JWTError, ValidationError):
        raise unauthorized

    data = UserTokenPayload(**payload)

    if data.username is None:
        raise unauthorized

    if (user := await get_user_by_username(data.username)) is None:
        raise unauthorized
    else:
        return user


USER = Depends(get_current_user)
