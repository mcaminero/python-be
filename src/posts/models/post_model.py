from src.posts.db.postgres_bd import Base
from sqlalchemy.types import Integer, String, TIMESTAMP, Boolean
from sqlalchemy import text, Column
from datetime import datetime


class Post(Base):
    __tablename__ = "posts"
    id: int = Column(Integer, primary_key=True, nullable=True)
    title: str = Column(String, nullable=False)
    content: str = Column(String, nullable=False)
    published: bool = Column(Boolean, server_default="TRUE")
    created_at: datetime = Column(
        TIMESTAMP(timezone=True), server_default=text("now()")
    )
