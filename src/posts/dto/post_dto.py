from pydantic import BaseModel
from datetime import datetime


class PostRequest(BaseModel):
    title: str
    content: str


class PostResponse(BaseModel):
    id: int
    title: str
    content: str
    published: bool
    created_at: datetime
