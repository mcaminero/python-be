from fastapi import HTTPException, status
from src.utils import utils
from fastapi import APIRouter
from pydantic import BaseModel
from src.posts.crud import post_crud
from src.posts.dto.post_dto import PostRequest, PostResponse
from src.deps import USER


class Message(BaseModel):
    message: str


route = APIRouter(prefix="/posts", tags=["Posts"])

route.dependencies = [USER]


@route.get("/", response_model=list[PostResponse])
async def get_posts() -> list[PostResponse]:
    return [utils.from_post(post) for post in post_crud.get_posts()]


@route.post("/", status_code=status.HTTP_201_CREATED, response_model=PostResponse)
async def create_posts(data: PostRequest) -> PostResponse:
    post = post_crud.create_post(data)
    return utils.from_post(post)


@route.put("/{post_id}", status_code=status.HTTP_200_OK, response_model=PostResponse)
async def update_post(post_id: int, request: PostRequest) -> PostResponse:
    post = post_crud.update_post(post_id=post_id, request=request)

    if post is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="There is no post with id: " + str(post_id),
        )

    return utils.from_post(post)


@route.delete(
    "/delete/{post_id}",
    responses={status.HTTP_404_NOT_FOUND: {"description": "No post found"}},
)
async def delete_post(post_id: int) -> None:
    return post_crud.delete_post(post_id)


@route.get("/{post_id}", status_code=status.HTTP_200_OK, response_model=PostResponse)
async def find_post_by_id(post_id: int) -> PostResponse:
    post = post_crud.get_post_by_id(post_id)
    if post == None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="There is no post with id: " + str(post_id),
        )
    return utils.from_post(post)
