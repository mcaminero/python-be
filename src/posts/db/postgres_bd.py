from sqlalchemy.engine.create import create_engine
from sqlalchemy.engine.url import URL
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.orm.decl_api import declarative_base
import os

# mejorar la coneccion a la base de datos

DATABASE_URL = os.getenv("DATABASE_URL", URL.create(drivername="db"))

engine = create_engine(DATABASE_URL)

session = sessionmaker(engine)

Base = declarative_base()
