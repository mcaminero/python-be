from src.posts.db.postgres_bd import session
from src.posts.models.post_model import Post
from datetime import datetime
from src.posts.dto.post_dto import PostRequest


def get_post_by_id(post_id: int):
    with session() as db_session:
        return db_session.get(Post, post_id)


def delete_post(post_id: int) -> None:
    with session() as db_session, db_session.begin():
        post = get_post_by_id(post_id)
        return db_session.delete(post)


def update_post(post_id: int, request: PostRequest) -> "Post|None":
    with session() as db_session:
        post = db_session.get(Post, post_id)
        if post is not None:
            post.title = request.title
            post.content = request.content
            post.created_at = datetime.now()
            db_session.commit()
            db_session.refresh(post)
        return post


def create_post(data: PostRequest) -> "Post":
    with session() as db_session:
        post = Post(**data.model_dump())
        db_session.add(post)
        db_session.commit()
        db_session.refresh(post)
        return post


def get_posts() -> list[Post]:
    with session() as db_session:
        return db_session.query(Post).order_by(Post.id).all()
