from src.posts.models.post_model import Post
from src.posts.dto.post_dto import PostResponse
from src.users.model.user_model import User
from src.users.dto.users_dto import UserResponse


def from_post(post: Post) -> "PostResponse":
    return PostResponse(
        id=post.id,
        title=post.title,
        content=post.content,
        created_at=post.created_at,
        published=post.published,
    )


def from_user(user: User) -> "UserResponse":
    return UserResponse(
        name=user.name,
        lastname=user.lastname,
        username=user.username,
    )
