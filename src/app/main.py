from fastapi import FastAPI
import psycopg2
from src.posts.db.postgres_bd import Base, engine

from src.posts.router import posts_router
from src.users.router import user_router

Base.metadata.create_all(bind=engine)

app = FastAPI()
routes = [posts_router.route, user_router.route]

for route in routes:
    app.include_router(route)
